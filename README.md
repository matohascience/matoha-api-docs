# Matoha API v1 Docs


## **DEPRECATED** - please see [Matoha API v2](https://app.swaggerhub.com/apis-docs/matoha/MatohaAPI/2.0.0) for the latest version containing many improvements.

API v1 will continue to function until April 2025. However, we recommend that you switch to API v2 as soon as possible.


## Access

To authenticate with our API server, you need a username and an API key. Whilst this API access is in beta, please contact
us to obtain these.

At present, the API is free to use but the users are requested to not make excessive requests / load on our servers.

## Documentation

Please see `API.md` for information on the API functions.

## Example source code

Please see the `examples` folder for Python and JavaScript examples as well as the `README.md` in this folder for
further instructions. We also provide a HTML demo which uses the JavaScript example - you can find it in
`examples/data_access_example.html` or deployed at [https://matohascience.gitlab.io/matoha-api-docs/](https://matohascience.gitlab.io/matoha-api-docs/).

## Support & contact

Please get in touch using the e-mail address hello at matoha dot com.

## License

The contents of this repository are licensed under the open-source MIT license.