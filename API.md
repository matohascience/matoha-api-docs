# API protocol documentation

## Server & protocol

The API is accessible via `https://api.matoha.com/`
Please ensure you use https for encrypting the communications (this is also enforced from our side).

To use the API, send a HTTP GET request to the server above. Each parameter shall be URL encoded, for instance:
`https://api.matoha.com/v1/get_data?collection=collection_id&format=csv` where the two parameters `collection` and
`format` are placed after the endpoint address (`/v1/get_data`).

You need to also authenticate every request - see below!

## Authentication

### Method 1 - HTTP BASIC (preferred)
HTTP BASIC authentication is required for every request. This is well-supported by browsers, HTTP libraries such as
Python [Requests](https://docs.python-requests.org/en/latest/) or HTTP request tools such as [Postman](https://www.postman.com/downloads/)

This authentication scheme uses a HTTP header `Authorization: Basic <base64 encoded userID:API key>`.

Please see the `examples/` for an example how to set this up for Python or Javascript requests.

### Method 2 - API key in URL
This method can be in some cases less secure - we recommend that you use Method 1. However, if for some reason you are
unable to, you can also authenticate by adding `&api_key=apikeyhere&user=useridhere` to your HTTP request.

## Retrieve user's collections

**Endpoint:** `/v1/get_collections`

**Parameters:**
- `supervisor` set to 1 to enable supervisor mode (retrieving supervisee's collections)

**Return format:** JSON-formatted list of user's collections.

The response shall contain the following fields:
- `_id` unique ID of the collection
- `name` user-entered name for the collection
- `materials` dictionary with the format `{material_id : material name, ...}`
- `blends` true/false if blends are supported by the current collection
- `QRCode` string, QR code or barcode for the collection
- `lastUpdated` UNIX timestamp of when then the last entry into this collection was made
- `owner` the user ID of the owner of the collection

**Example response:**

```json
[
  {
    "_id": "uniquecollectionid",
    "name": "Example fabrics collection",
    "materials": {
        "1": "cotton", "2": "polyester", "3": "wool", "4": "acrylic", "5": "nylon", "6": "elastane",
        "7": "silk", "8": "viscose", "9": "acetate"
    }
  },
  {
    "_id": "anothercollectionid",
    "name": "Example plastics collection",
    "materials": {
      "1": "PET", "10": "PA", "11": "PLA", "12": "PETG", "13": "PU", "14": "PVDF", "15": "PEEK", "16": "POM", 
      "17": "PAI", "18": "ASA", "19": "TPU", "2": "PE", "3": "PVC", "5": "PP", "50": "Paper", "6": "PS", "7": "PC", 
      "8": "ABS", "9": "PMMA"}
  }
]
```

## Retrieve user's instrument

**Endpoint:** `/v1/get_instruments`

**Parameters:**
- `supervisor` set to 1 to enable supervisor mode (retrieving supervisee's collections)

**Return format:** JSON-formatted list of user's collections.

The response shall contain the following fields:
- `serial_no` serial number of the machine
- `dislay_name` display name (can be changed in our app)
- `instrument_type` one of `plastell` or `fabritell`
- `owner` user ID of the device owner

**Example response:**

```json
[
  {
    "serial_no": "machineserial123",
    "display_name": "PlasTell 123",
    "instrument_type": "plastell",
    "owner": "someuser"
  }
]
```

## Retrieve data

**Endpoint:** `/v1/get_data`

**Parameters:**
- `collection` string with the unique collection ID (use `get_collections` to obtain it), may be `*` for all data
- `instrument` set to one of: device serial number or `plastell` or `fabritell` (for all Fabri/Plast devices)
- `format` export format, either `csv` or `json` (defaults to `json`)
- `limit` maximum number of data points to return (default and maximum value is 100 000 if compress = 0, 1 000 000 if compress = 1)
- `offset` to retrieve more than the `limit` number of records, e.g. if `limit` = 10 and `offset` = 20, the API will return records 21-30
- `compress` set to "1" to enable zip compression of the data
- `supervisor` set to 1 to enable supervisor mode (retrieving supervisee's collections)

**Return format:** JSON- or CSV-formatted list of data. The data are sorted by timestamp, in descending order.

*Response fields:*
- `_id` unique ID of the spectrum
- `materialsJSON` JSON representation of the saved composition. The format is `[{mat: <materialID>, perc: <percentage composition>}, ...]`
- `materials` Plain text representation of the saved composition
- `deviceAnswer` and `deviceAnswerJSON` - as `materials` and `materialsJSON` but these are the machine's answer, not user entered
- `comment` the description entered into the app
- `timestamp` UNIX timestamp when the measurement was saved (milliseconds since 01/01/1970)
- various fields with sample properties, such as `colour` - these should be self-explanatory

**Example response:**

*JSON:*
```json
[
    {
        "_id": "uniquespectrumid",
        "materials": "95% cotton 5% polyester",
        "timestamp": 1628956035749,
        "comment": "Some T-shirt",
        "serialNo": "machineserialno",
        "collection": "examplecollectionid",
        "materialJSON": [
            {"mat": 1, "perc": 95},
            {"mat": 2, "perc": 5}
        ]
    },
    {
      ...
    }
]
```

*CSV:*
```
index,materials,deviceAnswer,timestamp,comment,serialNo,collection,materialJSON
0,uniquespectrumid,95% cotton 5% polyester,2021-08-14 15:47:15.749,Some T-shirt,machineserialno,examplecollectionid,"[{'mat': 1, 'perc': 95}, {'mat': 2, 'perc': 5}]"
1,...
2,...
```

## Check connectivity

This simple function always returns "OK"; useful to check if the connection to the Matoha API works. 

**Endpoint:** `/v1/connection_check`

**Parameters:** none

**Return format:** "OK"


## Get statistics

**Endpoint:** `/v1/statistics`

**Parameters:**
- `collection` set to collection ID for filtering by collection or leave empty for all collections
- `instrument` set to one of: device serial number or `plastell` or `fabritell` (for all Fabri/Plast devices)
- `timestampFrom` if time-based filtering is required, set this to start time unix timestamp (in seconds)
- `timestampTo` if time-based filtering is required, set this to end time unix timestamp (in seconds)
- `supervisor` set to 1 to enable supervisor mode (retrieving supervisee's collections)

**Return format:** JSON list of statistics.

*Response fields:*
- `materialJSON` JSON representation of the saved composition. The format is `[{mat: <materialID>, perc: <percentage composition>}, ...]`
- `material` Plain text representation of the saved composition
- `count` Number of measurements for this composition

**Example response:**

*JSON:*
```json
[
  {
    "materialJSON": {
      "1": 100
    },
    "count": 123,
    "material": "PET"
  },
  {
    "materialJSON": {
      "3": 100
    },
    "count": 456,
    "material": "PVC"
  }
]
```
