import os
import requests

# Make sure you set these environmental variables! Please don't hardcode them for security reasons.
try:
    api_key = os.environ["API_KEY"]
    api_user_id = os.environ["API_USER_ID"]
except KeyError as e:
    print(f"Please set the following environmental variable: {e}")
    exit(-1)

api_server = "https://api.matoha.com"

def retrieve_collections(supervisor: bool = False) -> list:
    """
    Retrieves user collections.
    :param supervisor: use supervisor mode?
    :return: a List of collections
    """
    result = requests.get(f"{api_server}/v1/get_collections?supervisor={'1' if supervisor else '0'}", auth=(api_user_id, api_key))

    if result.status_code != 200:
        print(f"An error has occured when retrieving collections - \"{result.text}\" ({result.status_code})")
        return []

    # Decode the JSON response of the API server
    collections = []
    try:
        collections = result.json()
    except Exception as e:
        print(f"An error has occured when decoding the server's response: {e}")

    return collections


def retrieve_instruments(supervisor: bool = False) -> list:
    """
    Retrieves user instruments.
    :param supervisor: use supervisor mode?
    :return: a List of instruments
    """
    result = requests.get(f"{api_server}/v1/get_instruments?supervisor={'1' if supervisor else '0'}", auth=(api_user_id, api_key))

    if result.status_code != 200:
        print(f"An error has occured when retrieving instruments - \"{result.text}\" ({result.status_code})")
        return []

    # Decode the JSON response of the API server
    instruments = []
    try:
        instruments = result.json()
    except Exception as e:
        print(f"An error has occured when decoding the server's response: {e}")

    return instruments


def retrieve_collection_data(collection_id: str = "", instrument: str = "", limit: int = 100, supervisor: bool = False) -> list:
    """
    Retrieve data for a collection - list of saved spectra.

    :param collection_id: string with the unique ID.
    :param instrument: for filtering by instrument (type) - specify serial number or "plastell" or "fabritell"
    :param limit: the maximum number of data to retrieve, default to 100
    :param supervisor: use supervisor mode?
    :return: a list with the data
    """
    result = requests.get(f"{api_server}/v1/get_data?collection={collection_id}&limit={limit}&instrument={instrument}"
                          f"&supervisor={'1' if supervisor else '0'}", auth=(api_user_id, api_key))

    if result.status_code != 200:
        print(f"An error has occured when retrieving data for collection {collection_id} - \"{result.text}\" "
              f"({result.status_code})")
        return []

    # Decode the JSON response of the API server
    data = []
    try:
        data = result.json()
    except Exception as e:
        print(f"An error has occured when decoding the server's response: {e}")

    return data


def retrieve_statistics(instrument: str, collection: str = "", timestamp_from: int = 0,
                        timestamp_to: int = 3000000000, supervisor: bool = False) -> list:
    """
    Retrieve data for a collection - list of saved spectra.

    :param instrument: either the instrument serial number or "plastell" or "fabritell" to get all devices
    :param collection: string which can be empty (default) or contain a collection ID for data filtering
    :param timestamp_from: int with unix timestamp (in seconds), see https://www.unixtimestamp.com, for time-based filtering
    :param timestamp_to: as timestampFrom
    :param supervisor: use supervisor mode?
    :return: a list with the statistics, see the API.md for details

     apiServer + "/v1/statistics?supervisor=" + (supervisor ? "1" : "0") +
        "&instrument=" + instrument + "&collection=" + collection + "&timestampFrom=" + timestampFrom.toString() +
        "&timestampTo=" + timestampTo.toString();
    """
    result = requests.get(f"{api_server}/v1/statistics?instrument={instrument}&collection={collection}" 
                          f"&timestampFrom={str(timestamp_from)}&timestampTo={str(timestamp_to)}"
                          f"&supervisor={'1' if supervisor else '0'}", auth=(api_user_id, api_key))

    if result.status_code != 200:
        print(f"An error has occured when retrieving statistics - \"{result.text}\" "
              f"({result.status_code})")
        return []

    # Decode the JSON response of the API server
    data = []
    try:
        data = result.json()
    except Exception as e:
        print(f"An error has occured when decoding the server's response: {e}")

    return data


if __name__ == "__main__":
    print("Retrieving instruments...")
    instruments = retrieve_instruments(supervisor=True)
    print("Retrieved the following instruments:")
    if instruments:
        for instrument in instruments:
            print(instrument)

    print("\n\nRetrieving collections...")
    collections = retrieve_collections(supervisor=True)

    print("Retrieved the following collections:")
    if collections:
        for collection in collections:
            print(collection)

        print("\n\nRetrieving the first 3 data for the first collection...")
        data = retrieve_collection_data(collections[0]["_id"], limit=6, supervisor=True)
        print("Done, data:\n")
        for measurement in data:
            print(data)

        print("\n\nRetrieving measurement statistics for the first collection...")
        stats = retrieve_statistics(instrument="fabritell" if "blends" in collections[0] else "plastell",
                                    collection=collections[0]["_id"], supervisor=True)
        print("Done, stats:\n")
        for stat in stats:
            print(stat)

