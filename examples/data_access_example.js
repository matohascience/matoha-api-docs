
const apiServer = "https://api.matoha.com";

/**
 * Sends a request to the specified URL
 * Helper function used by the other methods in this file.
 *
 * @param userID User ID string
 * @param apiKey API key string
 * @param url str with the endpoint
 * @returns {Promise<string>} API response - either an error message or the list of collections with details
 */
async function sendApiRequest(userID, apiKey, url) {
    let myRequest = new Request(url);
    const headers = {"Authorization": 'Basic ' + btoa(userID + ":" + apiKey)};

    let responseText = "";
    try {
        const response = await fetch(myRequest, {headers: headers});
        // The JSON decoding then stringifying makes it format the JSON nicely for display
        // ...modify as needed
        responseText = await response.text();
        if(responseText[0] === "{" || responseText[0] === "[") { // Check if JSON-encoded
            responseText = JSON.stringify(JSON.parse(responseText), null, 4);
        }

        if (!response.ok) {
            return "ERROR " + response.status + "\n " + responseText;
        }
    } catch (e) {
        return "ERROR, request failed: " + JSON.stringify(e) + ". See your browser developer console for further messages.";
    }
    return responseText;
}


/**
 * Send a request to the get_collections endpoint to request a list of collections
 *
 * @param userID User ID string
 * @param apiKey API key string
 * @param supervisor bool use supervisor mode?
 * @returns  API response - either an error message or the list of collections with details
 */
async function getCollections(userID, apiKey, supervisor=false) {
    const url = apiServer + "/v1/get_collections?supervisor=" + (supervisor ? "1" : "0");
    return {result: await sendApiRequest(userID, apiKey, url), url: url};
}


/**
 * Send a request to the get_instruments endpoint to request a list of instruments
 *
 * @param userID User ID string
 * @param apiKey API key string
 * @param supervisor bool use supervisor mode?
 * @returns API response - either an error message or the list of collections with details
 */
async function getInstruments(userID, apiKey, supervisor=false) {
    const url = apiServer + "/v1/get_instruments?supervisor=" + (supervisor ? "1" : "0");
    return {result: await sendApiRequest(userID, apiKey, url), url: url};
}


/**
 * Retrieve data for a particular collection
 *
 * @param userID User ID string
 * @param apiKey API key string
 * @param collectionID ID of the collection (obtain this by getCollections())
 * @param instrument filter by instrument - serial number or "plastell" or "fabritell" to get all devices
 * @param dataFormat either "json" or "csv"
 * @param supervisor bool use supervisor mode?
 * @param limit maximum number of entries to return
 * @returns API response - either an error message or the list of data
 */
async function getData(userID, apiKey, collectionID, instrument, dataFormat, supervisor=false, limit=100) {
    const url = apiServer + "/v1/get_data?collection=" + collectionID + "&limit=" +limit + "&instrument=" + instrument +
    "&format=" + dataFormat + "&supervisor=" + (supervisor ? "1" : "0");
    return {result: await sendApiRequest(userID, apiKey, url), url: url};
}

/**
 * Retrieve measurement statistics
 *
 * @param userID User ID string
 * @param apiKey API key string
 * @param supervisor bool use supervisor mode?
 * @param instrument string which is either the instrument serial number or "plastell" or "fabritell" to get all devices
 * @param collection string which can be empty (default) or contain a collection ID for data filtering
 * @param timestampFrom int with unix timestamp (in seconds), see https://www.unixtimestamp.com, for time-based filtering
 * @param timestampTo as timestampFrom
 * @returns API response - either an error message or the statistics data
 */
async function getStatistics(userID, apiKey, supervisor=false, instrument="plastell", collection="",
                              timestampFrom=0, timestampTo=30000000000) {
    const url = apiServer + "/v1/statistics?supervisor=" + (supervisor ? "1" : "0") +
        "&instrument=" + instrument + "&collection=" + collection + "&timestampFrom=" + timestampFrom.toString() +
        "&timestampTo=" + timestampTo.toString();

    return {result: await sendApiRequest(userID, apiKey, url), url: url};
}


