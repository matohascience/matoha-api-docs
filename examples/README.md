# Matoha API examples

## Running Python examples

1. Please install Python 3.7 or newer.
2. Please install the required libraries `pip install -r requirements.txt`
3. To enable authentication, set the required environmental variables, `API_KEY` and `API_USER_ID`. On Mac/Linux,
`export API_KEY=yourapikey` and `export API_USER_ID=youruserid`
4. Run example `python data_access_example.py` and modify it as needed.

If you need any assistance, please don't hesistate to get in touch!

## Running JavaScript examples

The JS examples have only one dependency, the Fetch API which is typically included in all modern
browsers and also available for platforms such as Node and React - as such, this should work out of the box.

1. Open the `data_access_example.html` file in your web browser.
2. Put in your credentials and run as needed.